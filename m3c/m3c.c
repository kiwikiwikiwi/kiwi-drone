/*
 * m3c.c
 *
 *  Created on: 2015年6月30日
 *      Author: Harry
 */


#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"
#include "../includes/uart.h"
#include "../includes/m3c.h"

#define M3C_UART_BASE UART1_BASE

int16_t Gyro[3], Acc[3], Angle[3], Mag[3];
int32_t Altitude, Pressure;
float Temper, GyroDPS[3], AccG[3], MagGauss[3], AngleDeg[3];

uint8_t BufC;
uint16_t BufW;
uint8_t bufCheck,buf1,buf2,buf3,buf4,buf5,buf6,bufMCheck;

M3CValue * m3CValue;

uint8_t m3cAngleUpdateFlag = 0;

int8_t m3cUartRead(){
	return uart1Read();
}

int16_t m3cUartReadW(){
	return uart1ReadW();
}

void m3cInit(M3CValue * m3CValueP){
	m3CValue = m3CValueP;
}

void m3cReset(){
	UARTCharPutNonBlocking(UART1_BASE,0xD6);
	UARTCharPutNonBlocking(UART1_BASE,0x6D);
	UARTCharPutNonBlocking(UART1_BASE,0x01);
	UARTCharPutNonBlocking(UART1_BASE,0x01);
	SysCtlDelay(SysCtlClockGet()/3000*3000);
}

void m3cAngleReset(){
	UARTCharPutNonBlocking(UART1_BASE,0xD6);
	UARTCharPutNonBlocking(UART1_BASE,0x6D);
	UARTCharPutNonBlocking(UART1_BASE,0x10);
	UARTCharPutNonBlocking(UART1_BASE,0x10);
	SysCtlDelay(SysCtlClockGet()/3000*3000);
}

void m3cUpdateAngleBlock(){
	m3cAngleUpdateFlag = 0;

	while(!m3cAngleUpdateFlag){
		m3cUpdateAngle();
	}
}

void m3cUpdateAngle(){
	BufC = m3cUartRead();
	if(BufC==0xA7){
		BufC = m3cUartRead(); //读一个字节
		if (BufC==0x7A) { //判断是否为帧头
		  BufC = m3cUartRead(); //读一个字节
		  if(BufC == 0x72){
			  BufC = m3cUartRead(); //帧长度，可不用

			  bufCheck = m3cUartRead(); //效验位
			  buf1 = m3cUartRead();
			  buf2 = m3cUartRead();
			  buf3 = m3cUartRead();
			  buf4 = m3cUartRead();
			  buf5 = m3cUartRead();
			  buf6 = m3cUartRead();
			  bufMCheck = buf1^buf2^buf3^buf4^buf5^buf6;

			  BufW = (uint16_t)buf1;
			  Angle[0] = (int16_t)(((uint16_t)buf2  << 8) | BufW);
			  BufW = (uint16_t)buf3;
			  Angle[1] = (int16_t)(((uint16_t)buf4  << 8) | BufW);
			  BufW = (uint16_t)buf5;
			  Angle[2] = (int16_t)(((uint16_t)buf6  << 8) | BufW);

			  AngleDeg[0] = (float)Angle[0] / 100;
			  AngleDeg[1] = (float)Angle[1] / 100;
			  AngleDeg[2] = (float)Angle[2] / 100;
			  if (AngleDeg[2]<0) AngleDeg[2] += 360; //将航向值转化到0---360度区间

			  if(bufMCheck == bufCheck){
				  m3cAngleUpdateFlag = 1;

				  m3CValue->Angle[0] = Angle[0];
				  m3CValue->Angle[1] = Angle[1];
				  m3CValue->Angle[2] = Angle[2];
				  m3CValue->AngleDeg[0] = AngleDeg[0];
				  m3CValue->AngleDeg[1] = AngleDeg[1];
				  m3CValue->AngleDeg[2] = AngleDeg[2];
//				  UARTprintf("ANGLE X:%d  Y:%d  Z:%d \r\n", Angle[0], Angle[1], Angle[2]);
			  }else{
//				  m3CValue->Angle[0] = 0;
//				  m3CValue->Angle[1] = 0;
//				  m3CValue->Angle[2] = 0;
//				  m3CValue->AngleDeg[0] = 0;
//				  m3CValue->AngleDeg[1] =0;
//				  m3CValue->AngleDeg[2] = 0;
//				  UARTprintf("DROP [ANGLE X:%d  Y:%d  Z:%d] \r\n", Angle[0], Angle[1], Angle[2]);
			  }

		  }
		}
	}
}



void m3cGetAngleDeg(float *x, float *y, float *z)
{
	uint8_t BufC;
	if(UARTCharsAvail(M3C_UART_BASE)){
		BufC = m3cUartRead();
		if(BufC==0xA7){
			BufC = m3cUartRead(); //读一个字节
			if (BufC==0x7A) { //判断是否为帧头
			  BufC = m3cUartRead(); //读一个字节
			  if(BufC == 0x72){
				  BufC = m3cUartRead(); //帧长度，可不用
				  BufC = m3cUartRead(); //效验位
				  Angle[0] = m3cUartReadW();   //X轴角度(横滚)
				  Angle[1] = m3cUartReadW();   //Y轴角度(俯仰)
				  Angle[2] = m3cUartReadW();   //Z轴角度(偏航)

				  AngleDeg[0] = (float)Angle[0] / 100;
				  AngleDeg[1] = (float)Angle[1] / 100;
				  AngleDeg[2] = (float)Angle[2] / 100;
				  if (AngleDeg[2]<0) AngleDeg[2] += 360; //将航向值转化到0---360度区间

				  *x = AngleDeg[0];
				  *y = AngleDeg[1];
				  *z = AngleDeg[2];

			  }
		    }
		}
	}
}

void m3cGetAngle(int16_t *x, int16_t *y, int16_t *z){
	uint8_t BufC;
//	int8_t flag = 0;
//	while(!flag){
//		if(UARTCharsAvail(M3C_UART_BASE)){
			BufC = m3cUartRead();
			if(BufC==0xA7){
				BufC = m3cUartRead(); //读一个字节
				if (BufC==0x7A) { //判断是否为帧头
				  BufC = m3cUartRead(); //读一个字节
				  if(BufC == 0x72){
					  BufC = m3cUartRead(); //帧长度，可不用
					  BufC = m3cUartRead(); //效验位
					  Angle[0] = m3cUartReadW();   //X轴角度(横滚)
					  Angle[1] = m3cUartReadW();   //Y轴角度(俯仰)
					  Angle[2] = m3cUartReadW();   //Z轴角度(偏航)

					  AngleDeg[0] = (float)Angle[0] / 100;
					  AngleDeg[1] = (float)Angle[1] / 100;
					  AngleDeg[2] = (float)Angle[2] / 100;
					  if (AngleDeg[2]<0) AngleDeg[2] += 360; //将航向值转化到0---360度区间
					  UARTprintf("ANGLE X:%d  Y:%d  Z:%d \r\n", Angle[0], Angle[1], Angle[2]);
					  *x = Angle[0];
					  *y = Angle[1];
					  *z = Angle[2];
//					  flag = 1;
				  }
				}
			}
//		}
//	}
}

void m3cGetGyro(int16_t *x, int16_t *y, int16_t *z){
	uint8_t BufC;
	int8_t flag = 0;
	while(!flag){
		if(UARTCharsAvail(M3C_UART_BASE)){
			BufC = m3cUartRead();
			if(BufC==0xA7){
				BufC = m3cUartRead(); //读一个字节
				if (BufC==0x7A) { //判断是否为帧头
				  BufC = m3cUartRead(); //读一个字节
				  if(BufC == 0x70){
					  BufC = m3cUartRead(); //帧长度，可不用
					  BufC = m3cUartRead(); //效验位
					  Gyro[0] = m3cUartReadW();      //X轴
					  Gyro[1] = m3cUartReadW();      //Y轴
					  Gyro[2] = m3cUartReadW();      //Z轴

					  /*GYRO的量程为2000度每秒*/
					  /* 得到以"dps" ("度/秒")为单位的角速度值*/
					  GyroDPS[0] = (float)Gyro[0]*4/16.4;
					  GyroDPS[1] = (float)Gyro[1]*4/16.4;
					  GyroDPS[2] = (float)Gyro[2]*4/16.4;

					  *x = Gyro[0];      //X轴
					  *y = Gyro[1];      //Y轴
					  *z = Gyro[2];

					  UARTprintf("GYRO X:%d  Y:%d  Z:%d\r\n", Gyro[0], Gyro[1], Gyro[2]);
					  flag = 1;
				  }
				}
			}
		}
	}
}





void m3cUpdate(){
	      BufC = m3cUartRead(); //读一个字节
	      if((BufC==0xA7)){  //判断是否为帧 头
	        BufC = m3cUartRead(); //读一个字节
	        if (BufC==0x7A) { //判断是否为帧头
	          BufC = m3cUartRead(); //读一个字节
	          switch(BufC) //帧类型判断
	          {
	            case 0x70: //标识为角速度帧
	              BufC = m3cUartRead(); //帧长度，可不用
	              BufC = m3cUartRead(); //效验位
	              Gyro[0] = m3cUartReadW();      //X轴
	              Gyro[1] = m3cUartReadW();      //Y轴
	              Gyro[2] = m3cUartReadW();      //Z轴

	              /*GYRO的量程为2000度每秒*/
	              /* 得到以"dps" ("度/秒")为单位的角速度值*/
	              GyroDPS[0] = (float)Gyro[0]*4/16.4;
	              GyroDPS[1] = (float)Gyro[1]*4/16.4;
	              GyroDPS[2] = (float)Gyro[2]*4/16.4;

	              UARTprintf("GYRO X:%d  Y:%d  Z:%d\r\n", Gyro[0], Gyro[1], Gyro[2]);
	              break;
	            case 0x71: //标识为加速度帧
	              BufC = m3cUartRead(); //帧长度，可不用
	              BufC = m3cUartRead(); //效验位
	              Acc[0] = m3cUartReadW();      //X轴
	              Acc[1] = m3cUartReadW();      //Y轴
	              Acc[2] = m3cUartReadW();      //Z轴

	              /*ACC的量程为8G*/
	              /*得到以"g"为单位的加速度*/
	              AccG[0] = (float)Acc[0] / 4096;
	              AccG[1] = (float)Acc[1] / 4096;
	              AccG[2] = (float)Acc[2] / 4096;
	              UARTprintf("ACC X:%d  Y:%d  Z:%d\r\n", Acc[0], Acc[1], Acc[2]);
	              break;
	            case 0x72: //标识为姿态帧
	              BufC = m3cUartRead(); //帧长度，可不用

	              bufCheck = m3cUartRead(); //效验位
	              buf1 = m3cUartRead();
	              buf2 = m3cUartRead();
	              buf3 = m3cUartRead();
	              buf4 = m3cUartRead();
	              buf5 = m3cUartRead();
	              buf6 = m3cUartRead();
	              bufMCheck = buf1^buf2^buf3^buf4^buf5^buf6;

	              BufW = (uint16_t)buf1;
				  Angle[0] = (int16_t)(((uint16_t)buf2  << 8) | BufW);
				  BufW = (uint16_t)buf3;
				  Angle[1] = (int16_t)(((uint16_t)buf4  << 8) | BufW);
				  BufW = (uint16_t)buf5;
				  Angle[2] = (int16_t)(((uint16_t)buf6  << 8) | BufW);

				  AngleDeg[0] = (float)Angle[0] / 100;
				  AngleDeg[1] = (float)Angle[1] / 100;
				  AngleDeg[2] = (float)Angle[2] / 100;
				  if (AngleDeg[2]<0) AngleDeg[2] += 360; //将航向值转化到0---360度区间

	              if(bufMCheck == bufCheck){

					  UARTprintf("ANGLE X:%d  Y:%d  Z:%d \r\n", Angle[0], Angle[1], Angle[2]);
	              }else{
	            	  UARTprintf("DROP [ANGLE X:%d  Y:%d  Z:%d] \r\n", Angle[0], Angle[1], Angle[2]);
	              }

//	              Angle[0] = m3cUartReadW();   //X轴角度(横滚)
//	              Angle[1] = m3cUartReadW();   //Y轴角度(俯仰)
//	              Angle[2] = m3cUartReadW();   //Z轴角度(偏航)
//
//	              AngleDeg[0] = (float)Angle[0] / 100;
//	              AngleDeg[1] = (float)Angle[1] / 100;
//	              AngleDeg[2] = (float)Angle[2] / 100;
//	              if (AngleDeg[2]<0) AngleDeg[2] += 360; //将航向值转化到0---360度区间
//	              UARTprintf("ANGLE X:%d  Y:%d  Z:%d \r\n", Angle[0], Angle[1], Angle[2]);
	              break;
	            case 0x73: //标识为 地磁帧
	              BufC = m3cUartRead(); //帧长度，可不用
	              BufC = m3cUartRead(); //效验位
	              Mag[0] = m3cUartReadW();   //X轴
	              Mag[1] = m3cUartReadW();   //Y轴
	              Mag[2] = m3cUartReadW();   //Z轴

	              /*地磁设置为2.5Ga*/
	              /*得到以"Gauss"为单位的地磁*/
	              MagGauss[0] = (float)Mag[0] / 660;
	              MagGauss[1] = (float)Mag[1] / 660;
	              MagGauss[2] = (float)Mag[2] / 660;
	              UARTprintf("Mag X:%d  Y:%d  Z:%d \r\n", Mag[0], Mag[1], Mag[2]);
	              break;
	            case 0x74: //标识为温度、气压帧
	              BufC = m3cUartRead(); //帧长度，可不用
	              BufC = m3cUartRead(); //效验位
	              Temper = m3cUartReadW() / 10;   //X轴
	              BufW = m3cUartReadW();   //气压低16位
	              Pressure = (int32_t)(((uint32_t)(m3cUartReadW() << 16))|BufW);
//	              printf("Temperature(Degree)：%.2f  Pressure(Pa)：%d \r\n", Temper, Pressure);
	              UARTprintf("Pressure(Pa):%d \r\n", Temper, Pressure);
	              break;
	            case 0x75: // 标识为高度帧
	              BufC = m3cUartRead(); //帧长度，可不用
	              BufC = m3cUartRead(); //效验位
	              BufW = m3cUartReadW();   //高度低16位

	              /*得到以"CM"为单位的海拔高度*/
	              Altitude = (int32_t)(((uint32_t)(m3cUartReadW() << 16))|BufW);
	              UARTprintf("Altitude(cm):%d \r\n", Altitude);
	              break;
	            default:  break;
	          }
	        }
	      }
}







