//*****************************************************************************
// pwm_gen.c - Generate 4 PWM control signal
//
// Author: Jeff
//*****************************************************************************

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/rom.h"
#include "driverlib/gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/pin_map.h"
#include "../includes/pwm_gen.h"

//*****************************************************************************
// 
// PWMControlInit - Init the pwm generator controller
// \param psInst is a pointer to a tPWMControl instance
// \param fDuty0 is the duty of pwm generator 0
// \param fDuty1 is the duty of pwm generator 1
// \param fDuty2 is the duty of pwm generator 2
// \param fDuty3 is the duty of pwm generator 3
//
//*****************************************************************************
void PWMControlInit(tPWMControl *psInst, 
                    float fDuty0, 
                    float fDuty1, 
                    float fDuty2,
                    float fDuty3)
{
  // 
  // Save the parameters
  //
  psInst->afDutyCycle[0] = fDuty0;
  psInst->afDutyCycle[1] = fDuty1;
  psInst->afDutyCycle[2] = fDuty2;
  psInst->afDutyCycle[3] = fDuty3;
  psInst->ui32Period = SysCtlClockGet() / 64 / PWM_FREQ;

  // 
  // Enable the peripherals
  //
  ROM_SysCtlPeripheralEnable(PWM_M_PERIPH);
  ROM_SysCtlPeripheralEnable(PWM_GPIO_PERIPH);

  // 
  // GPIO config
  //
  ROM_GPIOPinTypePWM(PWM_GPIO_BASE_0, PWM_GPIO_PIN_0);
  ROM_GPIOPinTypePWM(PWM_GPIO_BASE_1, PWM_GPIO_PIN_1);
  ROM_GPIOPinTypePWM(PWM_GPIO_BASE_2, PWM_GPIO_PIN_2);
  ROM_GPIOPinTypePWM(PWM_GPIO_BASE_3, PWM_GPIO_PIN_3);
  ROM_GPIOPinConfigure(PWM_GPIO_CONFIG_0);
  ROM_GPIOPinConfigure(PWM_GPIO_CONFIG_1);
  ROM_GPIOPinConfigure(PWM_GPIO_CONFIG_2);
  ROM_GPIOPinConfigure(PWM_GPIO_CONFIG_3);

  // 
  // PWM clock set
  //
  ROM_SysCtlPWMClockSet(SYSCTL_PWMDIV_64);

  //
  // The mode of generator 0 and generator 1
  // Gen0 is associated with out0 and out1
  // Gen1 is associated with out2 and out3
  //
  PWMGenConfigure(PWM_M_BASE, PWM_GEN_0, 
                  PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC);
  PWMGenConfigure(PWM_M_BASE, PWM_GEN_1, 
                  PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC);

  // 
  // Period of gen0 and gen1
  //
  PWMGenPeriodSet(PWM_M_BASE, PWM_GEN_0, psInst->ui32Period);
  PWMGenPeriodSet(PWM_M_BASE, PWM_GEN_1, psInst->ui32Period);

  // 
  // Set pulse width
  //
  PWMPulseWidthSet(PWM_M_BASE, PWM_OUT_0, 
                   (uint32_t)(psInst->afDutyCycle[0] * psInst->ui32Period));
  PWMPulseWidthSet(PWM_M_BASE, PWM_OUT_1, 
                   (uint32_t)(psInst->afDutyCycle[1] * psInst->ui32Period));
  PWMPulseWidthSet(PWM_M_BASE, PWM_OUT_2, 
                   (uint32_t)(psInst->afDutyCycle[2] * psInst->ui32Period));
  PWMPulseWidthSet(PWM_M_BASE, PWM_OUT_3, 
                   (uint32_t)(psInst->afDutyCycle[3] * psInst->ui32Period));

  // 
  // Enable output
  //
  PWMOutputState(PWM_M_BASE, 
                 PWM_OUT_0_BIT | PWM_OUT_2_BIT |
                 PWM_OUT_1_BIT | PWM_OUT_3_BIT, true);

  // 
  // Enable generator
  //
  PWMGenEnable(PWM_M_BASE, PWM_GEN_0);
  PWMGenEnable(PWM_M_BASE, PWM_GEN_1);
}

//*****************************************************************************
//
// PWMControlDutyUpdate - Update the pwm duty cycle
// \param psInst is a pointer to a tPWMControl instance
// \param fDuty0 is the duty of pwm generator 0
// \param fDuty1 is the duty of pwm generator 1
// \param fDuty2 is the duty of pwm generator 2
// \param fDuty3 is the duty of pwm generator 3
//
//*****************************************************************************
void PWMControlDutyUpdate(tPWMControl *psInst, 
                          float fDuty0, 
                          float fDuty1, 
                          float fDuty2,
                          float fDuty3)
{
  // 
  // Save the parameters
  //
  psInst->afDutyCycle[0] = fDuty0;
  psInst->afDutyCycle[1] = fDuty1;
  psInst->afDutyCycle[2] = fDuty2;
  psInst->afDutyCycle[3] = fDuty3;

  // 
  // Set pulse width
  //
  PWMPulseWidthSet(PWM_M_BASE, PWM_OUT_0, 
                   (uint32_t)(psInst->afDutyCycle[0] * psInst->ui32Period));
  PWMPulseWidthSet(PWM_M_BASE, PWM_OUT_1, 
                   (uint32_t)(psInst->afDutyCycle[1] * psInst->ui32Period));
  PWMPulseWidthSet(PWM_M_BASE, PWM_OUT_2, 
                   (uint32_t)(psInst->afDutyCycle[2] * psInst->ui32Period));
  PWMPulseWidthSet(PWM_M_BASE, PWM_OUT_3, 
                   (uint32_t)(psInst->afDutyCycle[3] * psInst->ui32Period));
}


