//*****************************************************************************
//
// pid.c - PID controller.
//
// Author: Jeff
//*****************************************************************************

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include "../includes/pid.h"

#include "driverlib/uart.h"
#include "utils/uartstdio.h"

//*****************************************************************************
//
// Init the PID controller instance
// \param psInst is pointer to PID controller instance
// \param fKp is the proportional gain
// \param fKi is the integral gain
// \param fKd is the derivative gain
// \param ui32IntWindow is the integral calculate window size,
// \param fLimits is the output limits
// when the max calculate window is reached, abanden past integral.
//
//*****************************************************************************
void 
PIDControllerInit(tPIDController *psInst, 
                  float fKp,
                  float fKi, 
                  float fKd,
                  uint32_t ui32IntWindow,
                  float fLimits)
{
  // 
  // Init the PID gains
  //
  psInst->fKp = fKp;
  psInst->fKi = fKi;
  psInst->fKd = fKd;

  // 
  // The integral window size
  //
  psInst->ui32IntWindow = ui32IntWindow;

  // 
  // The limits
  //
  psInst->fLimits = fLimits;

  // 
  // Set other values to avoid accident
  //
  psInst->fSetValue = 0.0f;
  psInst->fSetValue = 0.0f;
  psInst->fPrevError = 0.0f;
  psInst->fOutput = 0.0f;
  psInst->ui32IntCnt = 0;
  psInst->fIntegral = 0.0f;
  psInst->pfnCallback = NULL;
  psInst->pvCallbackData = NULL;
}

//*****************************************************************************
//
// This function start the PID calculation.
// \param psInst is the pointer to the PID controller instance.
// \param fSetValue is the set target value
// \param fActualValue is the measured actual feedback value
// \param fTimeInterval is the time interval between each calculate.
// This parameter may infect the final output, 
// system clock count / system clock cycle
// is recommended.
// \param pfnCallback is the callback function.
// \param pvCallbackData is the callback data for the callback function.
//
//*****************************************************************************
void 
PIDCalc(tPIDController *psInst, 
        float fSetValue, 
        float fActualValue, 
        float fTimeInterval,
        tPIDCallback *pfnCallback, 
        void *pvCallbackData)
{

  // 
  // Calculate the error
  //
  float fError = fSetValue - fActualValue;

  // 
  // Save necessary parameters
  //
  psInst->fActualValue = fActualValue;
  psInst->fSetValue = fSetValue;
  psInst->pfnCallback = pfnCallback;
  psInst->pvCallbackData = pvCallbackData;

  // 
  // Calculate integral
  //
  if (psInst->ui32IntCnt >= psInst->ui32IntWindow)
  {
    psInst->fIntegral = 0.0f;
    psInst->ui32IntCnt = 0;
  }
  else
  {
    psInst->fIntegral += fError * fTimeInterval;
    psInst->ui32IntCnt = (psInst->ui32IntCnt + 1) % (psInst->ui32IntWindow + 1);
  }

  // 
  // Calculate the output 
  //
  psInst->fOutput = fError * psInst->fKp;
  psInst->fOutput += psInst->fIntegral * psInst->fKi;
  psInst->fOutput += psInst->fKd * fError / fTimeInterval;

  // 
  // The output should less than limits
  //
  if (psInst->fOutput > psInst->fLimits)
    psInst->fOutput = psInst->fLimits;
  else if (psInst->fOutput < - psInst->fLimits)
    psInst->fOutput = -psInst->fLimits;

  // 
  // Now call the callback
  //
  if (psInst->pfnCallback)
    psInst->pfnCallback(psInst->pvCallbackData);
}

//*****************************************************************************
//
// PIDGainUpdate - Update PID gains
// \param psInst is pointer to PID controller instance
// \param fKp is the proportional gain
// \param fKi is the integral gain
// \param fKd is the derivative gain
//
//*****************************************************************************
void PIDGainUpdate(tPIDController *psInst, 
                   float fKp,
                   float fKi, 
                   float fKd)
{
  // 
  // Update the PID gains
  //
  psInst->fKp = fKp;
  psInst->fKi = fKi;
  psInst->fKd = fKd;
}

//*****************************************************************************
//
// PIDMInit - Initilize tPIDMInstance instance
// \param psPIDMInst is pointer to the tPIDMInstance
// \param psPIDInst is pointer to six tPIDController instance
// !!! Global static variable is recommended !!!
//        0. X axis angle to generate Roll controll quantity
//        1. Y axis angle to generate Pitch controll quantity
//        2. Z axis angle to generate controll quantity
//        ...
// \param pfnCallback is the callback funtion to call after *this* operation
// \param pvCallbackData is data to callback function
//
//*****************************************************************************
void PIDMInit(tPIDMInstance *psPIDMInst, 
              tPIDController *psPIDInst,
              tPIDMCallback *pfnCallback,
              void *pvCallbackData)
{
  uint16_t i;

  // 
  // Save parameters
  //
  psPIDMInst->psPIDControllerInst = psPIDInst;
  psPIDMInst->pfnCallback = pfnCallback;
  psPIDMInst->pvCallbackData = pvCallbackData;

  // 
  // Initialize other values to avoid accident
  //
  for (i = 0; i < 4; i++)
    psPIDMInst->afControlSeq[i] = 0.0f;
  psPIDMInst->pfSensorEuler = NULL;
  psPIDMInst->pfVelocity = NULL;

  // 
  // Now callback
  //
  if (psPIDMInst->pfnCallback)
    psPIDMInst->pfnCallback(psPIDMInst->pvCallbackData);
}

//*****************************************************************************
//
// PIDMUpdateData - Update data from sensors and remote
// This should be called *before* PIDMCalc
// \param psPIDMInst is pointer to the tPIDMInstance
// \param pfRemoteControll is quantities from remote, global static recommended
// \param pfSensorEuler comes from sensor, global static recommended
// \param pfVelocity comes from sensor, global static recommended
// !!! psPIDMInst, pfRemoteControll and pfVelocity are recommended to be !!!
// !!! global variable !!!
// \param pfnCallback is the callback funtion to call after *this* operation
// \param pvCallbackData is data to callback function
//
//*****************************************************************************
void PIDMUpdateData(tPIDMInstance *psPIDMInst, 
                    float *pfRemoteControll,
                    float *pfSensorEuler, 
                    float *pfVelocity,
                    tPIDMCallback *pfnCallback,
                    void *pvCallbackData)
{
  // 
  // Save parameters
  //
  psPIDMInst->pfRemoteControll = pfRemoteControll;
  psPIDMInst->pfSensorEuler = pfSensorEuler;
  // TODO 
  // psPIDMInst->pfVelocity = pfVelocity;

  // 
  // Now callback
  //
  if (psPIDMInst->pfnCallback)
    psPIDMInst->pfnCallback(psPIDMInst->pvCallbackData);
}

//*****************************************************************************
//
// PIDMCalc - Get data from remote and calculate PID control sequences.
// !!! PIDMUpdateData must be called before to update sensor data. !!!
// \param psPIDMInst is pointer to the tPIDMInstance
// \param ui32Roll is control quantity from remote, referenced from fsi6.h
// \param ui32Pitch is control quantity from remote, referenced from fsi6.h
// \param ui32Throttle is control quantity from remote, referenced from fsi6.h
// \param ui32Yaw is control quantity from remote, referenced from fsi6.h
// \param pfnCallback is the callback funtion to call after *this* operation
// \param pvCallbackData is data to callback function
//
// Arrange of pfSensorEuler, afControlSeq, psPIDControllerInst are all same:
// - 0. X axis angle - Roll
// - 1. Y axis angle - Pitch
// - 2. Z axis angle - Yaw
// pfRemoteControll is the same but throttle set appended.
//
//*****************************************************************************
void PIDMCalc(tPIDMInstance *psPIDMInst, 
              float fTimeInterval,
              tPIDMCallback *pfnCallback, 
              void *pvCallbackData)
{
  uint16_t i;

  //
  // Save callback 
  //
  psPIDMInst->pfnCallback = pfnCallback;
  psPIDMInst->pvCallbackData = pvCallbackData;

  // 
  // Call function in psPIDControllerInst to calculate output
  //
  for (i = 0; i < 2; i++)
  {
    PIDCalc(&psPIDMInst->psPIDControllerInst[i], 
            psPIDMInst->pfRemoteControll[i], psPIDMInst->pfSensorEuler[i],
            fTimeInterval, NULL, NULL);
    psPIDMInst->afControlSeq[i] = psPIDMInst->psPIDControllerInst[i].fOutput;
  }

  //
  // Throttle
  //
  psPIDMInst->afControlSeq[3] = psPIDMInst->pfRemoteControll[3] * 100;

  // 
  // Now callback
  // Recommend: here to refresh pwm pulse
  //
  if (psPIDMInst->pfnCallback)
    psPIDMInst->pfnCallback(psPIDMInst->pvCallbackData);
}

