//*****************************************************************************
//
// pid.h - Prototypes for the pid controller.
//
// Author: Jeff
//*****************************************************************************

#ifndef PID_H_
#define PID_H_

typedef void (tPIDCallback)(void *pvData);
typedef void (tPIDMCallback)(void *pvData);

//*****************************************************************************
//
// tPIDController - PID controller struct 
//
//*****************************************************************************
typedef struct 
{
  // 
  // The proportional gain of the controller
  //
  float fKp;

  // 
  // The integral gain of the controller
  //
  float fKi;

  // 
  // The derivative gain of the controller
  //
  float fKd;

  // 
  // The set target value
  //
  float fSetValue;

  // 
  // The final output should less than this 
  //
  float fLimits;

  // 
  // The output value
  //
  float fOutput;

  // 
  // The actuall measured value
  //
  float fActualValue;

  // 
  // The integral window size
  //
  uint32_t ui32IntWindow;

  // 
  // The counter for integral window
  //
  uint32_t ui32IntCnt;

  // 
  // Error integral
  //
  float fIntegral;

  // 
  // Last preview error
  //
  float fPrevError;

  // 
  // The callback handler
  //
  tPIDCallback *pfnCallback;

  // 
  // The callback handler
  //
  void *pvCallbackData;
}
tPIDController;

//*****************************************************************************
//
// tPIDMInstance - The container of PID controller
//
//*****************************************************************************
typedef struct 
{
  // 
  // PID controller instances
  // Should be six of them
  // 0. X axis angle to generate Roll controll quantity
  // 1. Y axis angle to generate Pitch controll quantity
  // 2. Z axis angle to generate controll quantity
  //
  tPIDController *psPIDControllerInst;

  //
  // Four control sequence: roll, pitch, yaw, throttle
  //
  float afControlSeq[4];

  // 
  // Remote controll signals
  // Should be *Four* of them
  // 0. X axis angle set
  // 1. Y axis angle set
  // 2. Z axis angle set
  // 3. Throttle set
  //
  float *pfRemoteControll;

  // 
  // Euler angles from sensor
  // 0. X axis angle 
  // 1. Y axis angle 
  // 2. Z axis angle
  //
  float *pfSensorEuler;

  // 
  // Angular velocities from sensor
  //
  float *pfVelocity;

  // 
  // Callback function
  //
  tPIDMCallback *pfnCallback;

  // 
  // Callback data
  //
  void *pvCallbackData;
}
tPIDMInstance;

//*****************************************************************************
// 
// Prototypes.
//
//*****************************************************************************
void PIDControllerInit(tPIDController *psInst, float fKp,
                       float fKi, float fKd, 
                       uint32_t ui32IntWindow, float fLimits);

void PIDCalc(tPIDController *psInst, float fSetValue, 
             float fActualValue, float fTimeInterval,
             tPIDCallback *pfnCallback, void *pvCallbackData);

void PIDGainUpdate(tPIDController *psInst, float fKp,
                   float fKi, float fKd);

void PIDMInit(tPIDMInstance *psPIDMInst, 
              tPIDController *psPIDInst,
              tPIDMCallback *pfnCallback,
              void *pvCallbackData);

void PIDMUpdateData(tPIDMInstance *psPIDMInst, float *pfRemoteControll,
                    float *pfSensorEuler, float *pfVelocity,
                    tPIDMCallback *pfnCallback,
                    void *pvCallbackData);

void PIDMCalc(tPIDMInstance *psPIDMInst, 
              float fTimeInterval, tPIDMCallback *pfnCallback, 
              void *pvCallbackData);

#endif // PID_H_ 

