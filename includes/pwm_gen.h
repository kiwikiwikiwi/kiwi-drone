//*****************************************************************************
//
// pwm_gen.h - Prototypes and defines
//
// Author: Jeff
//*****************************************************************************

#ifndef PWM_GEN_H_
#define PWM_GEN_H_

//*****************************************************************************
//
// Configurations
//
//*****************************************************************************
#define PWM_FREQ        400

#define PWM_M_PERIPH    SYSCTL_PERIPH_PWM0
#define PWM_GPIO_PERIPH SYSCTL_PERIPH_GPIOB

#define PWM_M_BASE      PWM0_BASE
#define PWM_GPIO_BASE_0 GPIO_PORTB_BASE
#define PWM_GPIO_BASE_1 GPIO_PORTB_BASE
#define PWM_GPIO_BASE_2 GPIO_PORTB_BASE
#define PWM_GPIO_BASE_3 GPIO_PORTB_BASE

#define PWM_GPIO_PIN_0  GPIO_PIN_4
#define PWM_GPIO_PIN_1  GPIO_PIN_5
#define PWM_GPIO_PIN_2  GPIO_PIN_6
#define PWM_GPIO_PIN_3  GPIO_PIN_7

#define PWM_GPIO_CONFIG_0 GPIO_PB4_M0PWM2
#define PWM_GPIO_CONFIG_1 GPIO_PB5_M0PWM3
#define PWM_GPIO_CONFIG_2 GPIO_PB6_M0PWM0
#define PWM_GPIO_CONFIG_3 GPIO_PB7_M0PWM1

//*****************************************************************************
//
// tPWMControl
//
//*****************************************************************************
typedef struct
{
  // 
  // Duty cycle of four pwn generator
  //
  float afDutyCycle[4];

  //
  // Period
  //
  uint32_t ui32Period;
}
tPWMControl;

//*****************************************************************************
//
// Prototypes.
//
//*****************************************************************************
void PWMControlInit(tPWMControl *psInst, float fDuty0, 
                    float fDuty1, float fDuty2,
                    float fDuty3);

void PWMControlDutyUpdate(tPWMControl *psInst, float fDuty0, 
                          float fDuty1, float fDuty2,
                          float fDuty3);

#endif /* PWM_GEN_H_ */
