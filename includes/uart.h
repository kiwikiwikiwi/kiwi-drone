/*
 * uart.h
 */

#ifndef UART_H_
#define UART_H_

// UART configuration
#define M_UARTGPIO_PERIPH SYSCTL_PERIPH_GPIOA
#define M_UART_PERIPH SYSCTL_PERIPH_UART0

#define M_UARTGPIO_PIN_CONFIG_0 GPIO_PA0_U0RX
#define M_UARTGPIO_PIN_CONFIG_1 GPIO_PA1_U0TX
#define M_UARTGPIO_BASE GPIO_PORTA_BASE
#define M_UARTGPIO_PIN_0 GPIO_PIN_0
#define M_UARTGPIO_PIN_1 GPIO_PIN_1

#define M_UART_BASE UART0_BASE

void ConfigureUART(void);


void UART1Init(void);
int8_t uart1Read(void);
int16_t uart1ReadW(void);


#endif /* UART_H_ */
