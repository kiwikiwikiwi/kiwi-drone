/*
 * fsi6.h
 *
 *  Created on: 2015��7��1��
 *      Author: Harry
 */

#ifndef FSI6_H_
#define FSI6_H_

typedef struct
{
	int32_t ch1IntValue;
	int32_t ch2IntValue;
	int32_t ch3IntValue;
	int32_t ch4IntValue;
	float ch1Value;
	float ch2Value;
	float ch3Value;
	float ch4Value;
}FsValue;

void fsi6Init(FsValue *fsValue);
void fsi6BlockRead(void);

#endif /* FSI6_H_ */
