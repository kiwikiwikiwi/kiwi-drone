/*
 * m3c.h
 *
 *  Created on: 2015��6��30��
 *      Author: Harry
 */

#ifndef M3C_H_
#define M3C_H_

typedef struct
{
	int16_t Angle[3];
	float AngleDeg[3];
}M3CValue;

void m3cGetAngleDeg(float *x, float *y, float *z);
void m3cGetAngle(int16_t *x, int16_t *y, int16_t *z);
void m3cGetGyro(int16_t *x, int16_t *y, int16_t *z);
void m3cAngleReset(void);
void m3cUpdate(void);
void m3cUpdateAngle(void);
void m3cUpdateAngleBlock(void);

void m3cInit(M3CValue * m3CValueP);


#endif /* M3C_H_ */
