//*****************************************************************************
//
// main.c 
//
//*****************************************************************************

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/debug.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/interrupt.h"
#include "driverlib/timer.h"
#include "utils/uartstdio.h"
#include "../includes/pwm_gen.h"
#include "../includes/uart.h"
#include "../includes/pid.h"
#include "../includes/fsi6.h"
#include "../includes/m3c.h"

#define PID_CAL_FREQ 4

//*****************************************************************************
//
// Global instance to generate PWM 
//
//*****************************************************************************
tPWMControl g_sPWMInst;

//*****************************************************************************
//
// Global instance to get from remote 
//
//*****************************************************************************
FsValue g_sFSInst;

//*****************************************************************************
//
// Global instance for sensor m3c
//
//*****************************************************************************
M3CValue g_sM3CVInst;

//*****************************************************************************
//
// Global instance array to calculate PID
// 0. X axis angle to generate Roll controll quantity
// 1. Y axis angle to generate Pitch controll quantity
// 2. Z axis angle to generate controll quantity
//        ...
//
//*****************************************************************************
tPIDController g_asPIDCInst[6];

//*****************************************************************************
//
// Global instance of tPIDMInstance to control PID calculate
//
//*****************************************************************************
tPIDMInstance g_sPIDMInst;

//*****************************************************************************
//
// Global arrary to save remote control data
// Three Euler angles and a throttle appended
// - 0. X axis angle - Roll
// - 1. Y axis angle - Pitch
// - 2. Z axis angle - Yaw
// - 3. Throttle
//
//*****************************************************************************
float g_afRemoteControl[4];

//*****************************************************************************
//
// Global array to save Euler angles from sensors
// - 0. X axis angle - Roll
// - 1. Y axis angle - Pitch
// - 2. Z axis angle - Yaw
//
//*****************************************************************************
float g_afSensorEuler[3];

//*****************************************************************************
//
// Global array to save velocity data
// TODO
//
//*****************************************************************************

//*****************************************************************************
//
// PIDAppCallback
// TODO 
// Here to cast final PID result to PWM pulse width
//
//*****************************************************************************
void PIDAppCallback(void *pvData)
{
  uint16_t i;

  tPIDMInstance *psInst;
  float afDuty[4];
  psInst = pvData;
  afDuty[0] = 0.4 * (psInst->afControlSeq[3] - 
              psInst->afControlSeq[1] + 
              psInst->afControlSeq[0]) / 100.0 + 0.4;
  afDuty[1] = 0.4 * (psInst->afControlSeq[3] + 
              psInst->afControlSeq[1] + 
              psInst->afControlSeq[0]) / 100.0 + 0.4;
  afDuty[2] = 0.4 * (psInst->afControlSeq[3] + 
              psInst->afControlSeq[1] - 
              psInst->afControlSeq[0]) / 100.0 + 0.4;
  afDuty[3] = 0.4 * (psInst->afControlSeq[3] - 
              psInst->afControlSeq[1] - 
              psInst->afControlSeq[0]) / 100.0 + 0.4;
  for (i = 0; i < 4; i++)
  {
    if (afDuty[i] > 0.8f)
      afDuty[i] = 0.8f;
    else if (afDuty[i] < 0.4f)
      afDuty[i] = 0.4f;
  }

  PWMControlDutyUpdate(&g_sPWMInst, afDuty[0], afDuty[1], afDuty[2], afDuty[3]);
  /*
  UARTprintf("PWM updated: %4d %4d %4d %4d \r", (int)(afDuty[0] * 100), 
             (int)(afDuty[1] * 100), (int)(afDuty[2] * 100), 
             (int)(afDuty[3] * 100));
  */
}

//*****************************************************************************
//
// Timer int handler
// Calculate PID one time per interrupt
//
//*****************************************************************************
void PIDIntTimerHandler(void)
{
  uint16_t i;

  // 
  // Clear Timer int
  //
  ROM_TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

  // 
  // Read data from sensor driver
  //
  m3cUpdateAngleBlock();
  g_afSensorEuler[0] = g_sM3CVInst.AngleDeg[0];
  g_afSensorEuler[1] = g_sM3CVInst.AngleDeg[1];
  g_afSensorEuler[2] = g_sM3CVInst.AngleDeg[2];

  //
  // Calculate PID
  // 
  PIDMCalc(&g_sPIDMInst, 1, PIDAppCallback, &g_sPIDMInst);

  //UARTprintf("\r");
  for (i = 0; i < 4; i++)
  {
   //UARTprintf("%d ", (int)g_sPIDMInst.afControlSeq[i]);
  }
}

int main(void)
{

  // 
  // Enable lazy stacking for interrupt handlers
  //
  ROM_FPULazyStackingEnable();

  // 
  // 40MHz
  //
  ROM_SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN |
                     SYSCTL_XTAL_16MHZ);

  // 
  // Configure timer0 to work for PID calculate int handler
  //
  ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
  ROM_TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);
  ROM_TimerLoadSet(TIMER0_BASE, TIMER_A, ROM_SysCtlClockGet() / PID_CAL_FREQ);

  ROM_IntMasterEnable();
  ROM_IntEnable(INT_TIMER0A);
  ROM_TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
  TimerIntRegister(TIMER0_BASE, TIMER_A, PIDIntTimerHandler);

  //
  // Configure UART
  //
  ConfigureUART();

  // 
  // Config UART1 for m3c driver
  //
  UART1Init();

  // 
  // Init m3c driver
  //
  m3cInit(&g_sM3CVInst);
  m3cAngleReset();

  // 
  // Init FsValue instance
  //
  fsi6Init(&g_sFSInst);

  // 
  // Init PWM Controller instance
  //
  PWMControlInit(&g_sPWMInst, 0, 0, 0, 0);

  // 
  // Init PID relevant
  //
  PIDControllerInit(&g_asPIDCInst[0], 1, 0.0, 0.0, 2.0, 100.0);
  PIDControllerInit(&g_asPIDCInst[1], 1, 0.0, 0.0, 2.0, 100.0);
  PIDControllerInit(&g_asPIDCInst[2], 1, 0.0, 0.0, 2.0, 100.0);
  PIDControllerInit(&g_asPIDCInst[3], 1, 0.0, 0.0, 2.0, 100.0);
  PIDControllerInit(&g_asPIDCInst[4], 1, 0.0, 0.0, 2.0, 100.0);
  PIDControllerInit(&g_asPIDCInst[5], 1, 0.0, 0.0, 2.0, 100.0);

  PIDMInit(&g_sPIDMInst, g_asPIDCInst, NULL, NULL);

  // 
  // Start PID calculate
  //
  fsi6BlockRead();
  g_afRemoteControl[0] = 360.0 * (float)(g_sFSInst.ch1IntValue - 750) / 500.0;
  g_afRemoteControl[1] = 360.0 * (float)(g_sFSInst.ch2IntValue - 750) / 500.0;
  g_afRemoteControl[2] = 360.0 * (float)(g_sFSInst.ch4IntValue - 500) / 500.0;
  g_afRemoteControl[3] = (float)(g_sFSInst.ch3IntValue - 500) / 500.0;

  // 
  // Now update the PIDM data
  //
  PIDMUpdateData(&g_sPIDMInst, g_afRemoteControl, g_afSensorEuler, 
                 NULL, NULL, NULL);

  ROM_TimerEnable(TIMER0_BASE, TIMER_A);
  while (1)
  {
    fsi6BlockRead();
    // 
    // Read remote controll signal to calculate Euler angles
    // TODO
    g_afRemoteControl[0] = 360.0 * (float)(g_sFSInst.ch1IntValue - 750) / 500.0;
    g_afRemoteControl[1] = 360.0 * (float)(g_sFSInst.ch2IntValue - 750) / 500.0;
    g_afRemoteControl[2] = 360.0 * (float)(g_sFSInst.ch4IntValue - 500) / 500.0;
    g_afRemoteControl[3] = (float)(g_sFSInst.ch3IntValue - 500) / 500.0;
  }
}

