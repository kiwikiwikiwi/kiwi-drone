/*
 * uart.c
 */
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"
#include "../includes/uart.h"

void ConfigureUART(void)
{
  SysCtlPeripheralEnable(M_UARTGPIO_PERIPH);
  SysCtlPeripheralEnable(M_UART_PERIPH);

  GPIOPinConfigure(M_UARTGPIO_PIN_CONFIG_0);
  GPIOPinConfigure(M_UARTGPIO_PIN_CONFIG_1);

  GPIOPinTypeUART(M_UARTGPIO_BASE, M_UARTGPIO_PIN_0 | M_UARTGPIO_PIN_1);

  UARTClockSourceSet(M_UART_BASE, UART_CLOCK_PIOSC);
  UARTStdioConfig(0, 115200, 16000000);
}

void UART1Init(void)
{
	//使能端口A 使能外设串口0
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);

	//配置引脚功能
	GPIOPinConfigure(GPIO_PB0_U1RX);
	GPIOPinConfigure(GPIO_PB1_U1TX);

	//设置引脚模式
	GPIOPinTypeUART(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1);

	//设置串口0时钟源，片内16M精准晶振
	UARTClockSourceSet(UART1_BASE, UART_CLOCK_PIOSC);

	//配置串口0，波特率，数据长度，停止位，校验位
	UARTConfigSetExpClk(UART1_BASE,16000000, 115200,(UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));
}

int8_t uart1Read(void)
{
	unsigned char BufC;
//	int32_t buf32;
//	while(!UARTCharsAvail(UART1_BASE)){
//
//	}
//	buf32 = UARTCharGetNonBlocking(UART1_BASE);
//	BufC = buf32;

	BufC = UARTCharGet(UART1_BASE);
	return BufC;
}

int16_t uart1ReadW(void)
{
	unsigned char BufC;
	int16_t BufW;

	BufC = uart1Read();
	BufW = (uint16_t)BufC;
	BufC = uart1Read();
	BufW = (int16_t)(((uint16_t)BufC  << 8) | BufW);
	return BufW;
}
