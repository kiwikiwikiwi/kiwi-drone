/*
 * fsi6.c
 *
 *  Created on: 2015年7月1日
 *      Author: Harry
 */


#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"
#include "driverlib/debug.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/timer.h"
#include "utils/uartstdio.h"
#include "../includes/fsi6.h"

#define     GPIO_PORT_BASE_CH1 GPIO_PORTC_BASE
#define           GPIO_PIN_CH1 GPIO_PIN_4
#define       GPIO_INT_PIN_CH1 GPIO_INT_PIN_4
#define       GPIO_CH1_WCCP GPIO_PC4_WT0CCP0

#define     GPIO_PORT_BASE_CH2 GPIO_PORTC_BASE
#define           GPIO_PIN_CH2 GPIO_PIN_5
#define       GPIO_INT_PIN_CH2 GPIO_INT_PIN_5
#define       GPIO_CH2_WCCP GPIO_PC5_WT0CCP1

#define     GPIO_PORT_BASE_CH3 GPIO_PORTC_BASE
#define           GPIO_PIN_CH3 GPIO_PIN_6
#define       GPIO_INT_PIN_CH3 GPIO_INT_PIN_6
#define       GPIO_CH3_WCCP GPIO_PC6_WT1CCP0

#define     GPIO_PORT_BASE_CH4 GPIO_PORTC_BASE
#define           GPIO_PIN_CH4 GPIO_PIN_7
#define       GPIO_INT_PIN_CH4 GPIO_INT_PIN_7
#define       GPIO_CH4_WCCP GPIO_PC7_WT1CCP1

#define PERIOD 800017

#define TIME_BASE_FS  TIMER0_BASE
#define TIME_FS  TIMER_A

uint32_t ch1Up = 0, ch1Down = 0,ch2Up = 0,ch2Down = 0;
uint32_t ch3Up = 0,ch3Down = 0,ch4Up = 0,ch4Down = 0;
uint32_t chWidth;
FsValue *mFsValue;

uint32_t timer0IntStatus;
uint32_t timer1IntStatus;

//uint16_t count=0;

uint8_t fsi6ReadFlag = 0;

void Timer0AIntHandler(void){
	TimerIntClear(WTIMER0_BASE, TIMER_CAPA_EVENT);

	if(GPIOPinRead(GPIO_PORT_BASE_CH1,GPIO_PIN_CH1) & GPIO_PIN_CH1){
		ch1Up = TimerValueGet(WTIMER0_BASE,TIMER_A);
	}else{
		ch1Down = TimerValueGet(WTIMER0_BASE,TIMER_A);
		mFsValue->ch1IntValue = (ch1Down - ch1Up)*10000/PERIOD;
		mFsValue->ch1Value = (float)(mFsValue->ch1IntValue)/10000;
		fsi6ReadFlag|=0x01;
//		UARTprintf("\n [CH1-READ]");
//		if(count<50){
//			count++;
//			UARTprintf("\r mFsValue->ch1Value %6d", mFsValue->ch1Value);
//		}

	}

}

void Timer0BIntHandler(void){
	TimerIntClear(WTIMER0_BASE, TIMER_CAPB_EVENT);

	if(GPIOPinRead(GPIO_PORT_BASE_CH2,GPIO_PIN_CH2)){
		ch2Up = TimerValueGet(WTIMER0_BASE,TIMER_B);
	}else{
		ch2Down = TimerValueGet(WTIMER0_BASE,TIMER_B);
		mFsValue->ch2IntValue = (ch2Down - ch2Up)*10000/PERIOD;
		mFsValue->ch2Value = (float)(mFsValue->ch2IntValue)/10000;
		fsi6ReadFlag|=0x02;
//		UARTprintf("\n [CH2-READ]");
	}

}

void Timer1AIntHandler(void){
	TimerIntClear(WTIMER1_BASE, TIMER_CAPA_EVENT);

	if(GPIOPinRead(GPIO_PORT_BASE_CH3,GPIO_PIN_CH3)){
		ch3Up = TimerValueGet(WTIMER1_BASE,TIMER_A);
	}else{
		ch3Down = TimerValueGet(WTIMER1_BASE,TIMER_A);
		mFsValue->ch3IntValue = (ch3Down - ch3Up)*10000/PERIOD;
		mFsValue->ch3Value = (float)(mFsValue->ch3IntValue)/10000;
		fsi6ReadFlag|=0x04;
//		UARTprintf("\n [CH3-READ]");
	}
}

void Timer1BIntHandler(void){
	TimerIntClear(WTIMER1_BASE, TIMER_CAPB_EVENT);

	if(GPIOPinRead(GPIO_PORT_BASE_CH4,GPIO_PIN_CH4)){
		ch4Up = TimerValueGet(WTIMER1_BASE,TIMER_B);
	}else{
		ch4Down = TimerValueGet(WTIMER1_BASE,TIMER_B);
		mFsValue->ch4IntValue = (ch4Down - ch4Up)*10000/PERIOD;
		mFsValue->ch4Value = (float)(mFsValue->ch4IntValue)/10000;
		fsi6ReadFlag|=0x08;
//		UARTprintf("\n [CH4-READ]");
	}
}

void fsi6Init(FsValue *fsValue){
	mFsValue = fsValue;

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);//使能GPIOF和GPIOC外设
    SysCtlPeripheralEnable(SYSCTL_PERIPH_WTIMER0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_WTIMER1);

	GPIOPinTypeGPIOInput(GPIO_PORTC_BASE, GPIO_PIN_CH1);
	GPIOPinTypeTimer(GPIO_PORTC_BASE, GPIO_PIN_CH1);
	GPIOPinConfigure(GPIO_CH1_WCCP);

	GPIOPinTypeGPIOInput(GPIO_PORTC_BASE, GPIO_PIN_CH2);
	GPIOPinTypeTimer(GPIO_PORTC_BASE, GPIO_PIN_CH2);
	GPIOPinConfigure(GPIO_CH2_WCCP);

	TimerConfigure(WTIMER0_BASE, (TIMER_CFG_A_CAP_TIME_UP|TIMER_CFG_B_CAP_TIME_UP | TIMER_CFG_SPLIT_PAIR)); //计数捕获模式，上升沿捕获，Two half-width timers
	TimerControlEvent(WTIMER0_BASE,TIMER_A, TIMER_EVENT_BOTH_EDGES);//捕获模式，A定时器，上升沿捕获,增计数模式，到达匹配值后可自动清零
	TimerIntRegister(WTIMER0_BASE,TIMER_A,Timer0AIntHandler);
	IntEnable(INT_WTIMER0A);	 //使能TIMER0A
	TimerIntEnable(WTIMER0_BASE, TIMER_CAPA_EVENT); //定时器A捕获事件触发中断
//	TimerEnable(WTIMER0_BASE, TIMER_A); //TIMER0A开始计数，当计数值等于TimerLoadSet，触发中断

	TimerControlEvent(WTIMER0_BASE,TIMER_B, TIMER_EVENT_BOTH_EDGES);//捕获模式，A定时器，上升沿捕获,增计数模式，到达匹配值后可自动清零
	TimerIntRegister(WTIMER0_BASE,TIMER_B,Timer0BIntHandler);
	IntEnable(INT_WTIMER0B);	 //使能TIMER0A
	TimerIntEnable(WTIMER0_BASE, TIMER_CAPB_EVENT); //定时器A捕获事件触发中断
//	TimerEnable(WTIMER0_BASE, TIMER_B); //TIMER0A开始计数，当计数值等于TimerLoadSet，触发中断

	GPIOPinTypeGPIOInput(GPIO_PORTC_BASE, GPIO_PIN_CH3);
	GPIOPinTypeTimer(GPIO_PORTC_BASE, GPIO_PIN_CH3);
	GPIOPinConfigure(GPIO_CH3_WCCP);

	GPIOPinTypeGPIOInput(GPIO_PORTC_BASE, GPIO_PIN_CH4);
	GPIOPinTypeTimer(GPIO_PORTC_BASE, GPIO_PIN_CH4);
	GPIOPinConfigure(GPIO_CH4_WCCP);

	TimerConfigure(WTIMER1_BASE, (TIMER_CFG_A_CAP_TIME_UP| TIMER_CFG_B_CAP_TIME_UP | TIMER_CFG_SPLIT_PAIR)); //计数捕获模式，上升沿捕获，Two half-width timers
	TimerControlEvent(WTIMER1_BASE,TIMER_A, TIMER_EVENT_BOTH_EDGES);//捕获模式，A定时器，上升沿捕获,增计数模式，到达匹配值后可自动清零
	TimerIntRegister(WTIMER1_BASE,TIMER_A,Timer1AIntHandler);
	IntEnable(INT_WTIMER1A);	 //使能TIMER0A
	TimerIntEnable(WTIMER1_BASE, TIMER_CAPA_EVENT); //定时器A捕获事件触发中断
//	TimerEnable(WTIMER1_BASE, TIMER_A); //TIMER0A开始计数，当计数值等于TimerLoadSet，触发中断

	TimerControlEvent(WTIMER1_BASE,TIMER_B, TIMER_EVENT_BOTH_EDGES);//捕获模式，A定时器，上升沿捕获,增计数模式，到达匹配值后可自动清零
	TimerIntRegister(WTIMER1_BASE,TIMER_B,Timer1BIntHandler);
	IntEnable(INT_WTIMER1B);	 //使能TIMER0A
	TimerIntEnable(WTIMER1_BASE, TIMER_CAPB_EVENT); //定时器A捕获事件触发中断
//	TimerEnable(WTIMER1_BASE, TIMER_B); //TIMER0A开始计数，当计数值等于TimerLoadSet，触发中断

}


void fsi6StopRead(){
//	TimerIntDisable(WTIMER0_BASE, TIMER_CAPA_EVENT);
	TimerDisable(WTIMER0_BASE, TIMER_A);
//	TimerIntDisable(WTIMER0_BASE, TIMER_CAPB_EVENT);
	TimerDisable(WTIMER0_BASE, TIMER_B);
//	TimerIntDisable(WTIMER1_BASE, TIMER_CAPA_EVENT);
	TimerDisable(WTIMER1_BASE, TIMER_A);
//	TimerIntDisable(WTIMER1_BASE, TIMER_CAPB_EVENT);
	TimerDisable(WTIMER1_BASE, TIMER_B);
}

void fsi6BlockRead(){
	fsi6ReadFlag = 0;
//	TimerIntEnable(WTIMER0_BASE, TIMER_CAPA_EVENT);
	TimerEnable(WTIMER0_BASE, TIMER_A);
//	TimerIntEnable(WTIMER0_BASE, TIMER_CAPB_EVENT);
	TimerEnable(WTIMER0_BASE, TIMER_B);
//	TimerIntEnable(WTIMER1_BASE, TIMER_CAPA_EVENT);
	TimerEnable(WTIMER1_BASE, TIMER_A);
//	TimerIntEnable(WTIMER1_BASE, TIMER_CAPB_EVENT);
	TimerEnable(WTIMER1_BASE, TIMER_B);
	while(fsi6ReadFlag!=0x0F){}
	fsi6StopRead();
}











